import React from 'react';
import { View } from 'react-native';
import {
	Container,
	Content,
	Text,
	Form,
	Item,
	Input,
	Label,
	Button
} from 'native-base';

export default class Register extends React.Component {
	render() {
		return (
			<Container>
				<Content
					style={{
						marginTop: 100
					}}
				>
					<View style={{ alignItems: 'center' }}>
						<Text style={{ fontSize: 35 }}>Register</Text>
					</View>
					<Form>
						<Item floatingLabel>
							<Label>Username</Label>
							<Input />
						</Item>
						<Item floatingLabel>
							<Label>Password</Label>
							<Input />
						</Item>
					</Form>

					<View style={{ marginTop: 30, alignItems: 'center' }}>
						<Button
							primary
							rounded
							full
							onPress={() => this.props.navigation.navigate('Login')}
						>
							<Text> Register </Text>
						</Button>

						<Button
							light
							rounded
							full
							onPress={() => this.props.navigation.navigate('Login')}
						>
							<Text> Back to Login </Text>
						</Button>
					</View>
				</Content>
			</Container>
		);
	}
}
