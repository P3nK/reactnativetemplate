import React, { Component } from 'react';
import Login from './Login.js';
import Register from './Register.js';
import HomeScreen from '../HomeScreen/index';
import { createSwitchNavigator } from 'react-navigation';
export default createSwitchNavigator(
	{
		Login: { screen: Login },
		Register: { screen: Register },
		HomeScreen: { screen: HomeScreen }
	},
	{
		headerMode: 'none',
		navigationOptions: {
			headerVisible: false
		}
	}
);
