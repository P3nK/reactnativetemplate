import React from 'react';
import { View } from 'react-native';
import {
	Container,
	Content,
	Text,
	Form,
	Item,
	Input,
	Label,
	Button
} from 'native-base';
import Register from './Register';
import firebase from '../../Utils/Firebase';

export default class Login extends React.Component {
	constructor() {
		super();
		this.state = {
			name: ''
		};
	}

	componentDidMount() {
		firebase
			.firestore()
			.collection('people')
			.onSnapshot(r => {
				console.log(r.docs[0].data().name);
				r.forEach(function(doc) {
					console.log(doc.data().name);
				});
				this.setState({ name: r.docs[0].data().name });
			});
	}

	render() {
		return (
			<Container>
				<Content
					style={{
						marginTop: 100
					}}
				>
					<View style={{ alignItems: 'center' }}>
						<Text style={{ fontSize: 35 }}>Login {this.state.name}</Text>
					</View>
					<Form>
						<Item floatingLabel>
							<Label>Username</Label>
							<Input />
						</Item>
						<Item floatingLabel>
							<Label>Password</Label>
							<Input />
						</Item>
					</Form>

					<View style={{ marginTop: 30, alignItems: 'center' }}>
						<Button
							primary
							rounded
							full
							onPress={() => this.props.navigation.navigate('HomeScreen')}
						>
							<Text> Login </Text>
						</Button>

						<Button
							light
							rounded
							full
							onPress={() => this.props.navigation.navigate('Register')}
						>
							<Text> Register </Text>
						</Button>

						<Button
							warning
							rounded
							full
							onPress={() => {
								// var provider = new firebase.auth.GoogleAuthProvider();

								firebase
									.auth()
									.signInWithPopup(new firebase.auth.GoogleAuthProvider())
									.then(function(result) {
										// This gives you a Google Access Token. You can use it to access the Google API.
										var token = result.credential.accessToken;
										// The signed-in user info.
										var user = result.user;
										// ...

										console.log('token => ', token);
										console.log('user => ', user);
									});
							}}
						>
							<Text> Login with Google </Text>
						</Button>
					</View>
				</Content>
			</Container>
		);
	}
}
