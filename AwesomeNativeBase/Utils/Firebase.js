import * as firebase from 'firebase';
import firestore from 'firebase/firestore';

const config = {
	apiKey: 'AIzaSyBkv9Oc2bZC6UA9MEsRGrh8C95BfwHkCoY',
	authDomain: 'siam-smile-lab.firebaseapp.com',
	databaseURL: 'https://siam-smile-lab.firebaseio.com',
	projectId: 'siam-smile-lab',
	storageBucket: 'siam-smile-lab.appspot.com',
	messagingSenderId: '83689345275'
};
firebase.initializeApp(config);

firebase.firestore().settings({});

export default (!firebase.apps.length
	? firebase.initializeApp(config)
	: firebase.app());
